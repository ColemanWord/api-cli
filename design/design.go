package design

import (
        . "github.com/goadesign/goa/design"
        . "github.com/goadesign/goa/design/apidsl"
)

var _ = API("word", func() {
        Title("The Word API")
        Description("A Word Service API")
        Host("localhost:8080")
        Scheme("http")
})

var _ = Resource("operands", func() {
        Action("show", func() {
                Routing(GET("show/:word"))
                Description("returns a word from the English dictionary")
                Params(func() {
                        Param("word", String, "word to be returned")
                })
                Response(OK, "text/plain")
        })

})
